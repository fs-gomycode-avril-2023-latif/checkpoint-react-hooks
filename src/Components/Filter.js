import React from "react";

const Filter = ({ handleFilterChange }) => {
  return (
    <div className="filter">
      <input
        type="text"
        placeholder="Filtrer par titre"
        onChange={(event) => handleFilterChange(event.target.value)}
      />
    </div>
  );
};

export default Filter;
