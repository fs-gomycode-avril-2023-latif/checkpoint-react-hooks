import React, { useState } from "react";
import MovieList from "./Components/MovieList";
import Filter from "./Components/Filter";

const App = () => {
  const [movies, setMovies] = useState([
    {
      title: "Film 1",
      description: "Description du film 1",
      posterURL: "https://example.com/poster1.jpg",
      note: 4.5,
    },
    {
      title: "Film 2",
      description: "Description du film 2",
      posterURL: "https://example.com/poster2.jpg",
      note: 3.8,
    },
    // Ajoutez d'autres films ici
  ]);

  const [filteredMovies, setFilteredMovies] = useState(movies);

  const handleFilterChange = (filterValue) => {
    const filtered = movies.filter((movie) =>
      movie.title.toLowerCase().includes(filterValue.toLowerCase())
    );
    setFilteredMovies(filtered);
  };

  const handleAddMovie = (newMovie) => {
    setMovies([...movies, newMovie]);
  };

  return (
    <div className="app">
      <h1>Liste des films</h1>
      <Filter handleFilterChange={handleFilterChange} />
      <MovieList movies={filteredMovies} />
      {/* Composant pour ajouter un nouveau film */}
    </div>
  );
};

export default App;
